;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-latex-src-block-backend . engraved)
              (indent-tabs-mode . nil)
              (require-final-newline . t)))
 (find-file . ((show-trailing-whitespace . t))))
